//
//  VentanaTableViewCell2.swift
//  Ventana
//
//  Created by Erick Cruz on 7/3/17.
//  Copyright © 2017 Erick Cruz. All rights reserved.
//

import UIKit

class VentanaTableViewCell2: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
