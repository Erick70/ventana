//
//  ViewController2.swift
//  Ventana
//
//  Created by Erick Cruz on 7/2/17.
//  Copyright © 2017 Erick Cruz. All rights reserved.
//

import UIKit

class ViewController2: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var ChangeTableViewController: VentanaTableViewCell2!

    let list = ["Current Password","New Password", "Confirm new Password"]
    let listB = ["Please Enter","Minimun 8 Character", "Confirm new Password"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (list.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell" )
        cell.textLabel?.text = list[indexPath.row]
        
        return cell
    }
    
}
