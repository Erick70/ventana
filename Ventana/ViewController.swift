//
//  ViewController.swift
//  Ventana
//
//  Created by Erick Cruz on 7/2/17.
//  Copyright © 2017 Erick Cruz. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var VentanaTableView: UITableView!
    
    @IBOutlet weak var imageCell: UIImageView!

    let list = ["First Name","Last Name", "Height", "Country", "Email Address", "Telephone Number"]
    let listB = ["Barack", "Obama", "1.83 m", "EEUU", "barack@hotmail.com", "0987654321"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (list.count)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell" )
        
       // let cell = self.VentanaTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VentanaTableViewCell
        //cell.textLabel?.text = list[indexPath.row]
        //cell.ventanaLabel?.text = list[indexPath.row]
        //cell.descriptionLabel?.text = listB[indexPath.row]
        
        cell.textLabel?.text = list[indexPath.row]
        return cell
    }


}

